#!/bin/bash

# emmabuntus_ventoy_exec.sh --
#
#   This file permits to launch Ventoy
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

dir=/opt/ventoy
config_file=Ventoy2Disk.ini
generate_file="true"

# Teste si le répertoire existe
if test -d ${dir} ; then

# Test si le fichier de config n'existe pas

if ! test -f ${dir}/${config_file} ; then


if [[ $LANG == fr* ]] ; then

    config_language_english="French"
    config_language="Français"

elif [[ $LANG == de* ]] ; then

    config_language_english="German"
    config_language="Deutsch"


elif [[ $LANG == pt* ]] ; then

    config_language_english="Portuguese"
    config_language="Português de Portugal"

elif [[ $LANG == it* ]] ; then

    config_language_english="Italian"
    config_language="Italiano"


elif [[ $LANG == es* ]] ; then

    config_language_english="Spanish"
    config_language="Español"


else

    generate_file="false"

fi


if [[ ${generate_file} == "true" ]] ; then

sudo tee ${dir}/${config_file} > /dev/null <<EOT
[Ventoy]
Language=
PartStyle=0
ShowAllDevice=0
EOT

sudo chmod a+r ${dir}/${config_file}

sed s/Language=/Language=${config_language_english}\ \(${config_language}\)/ ${dir}/${config_file} | sudo tee ${dir}/${config_file}.tmp

sudo cp ${dir}/${config_file}.tmp ${dir}/${config_file}
sudo rm ${dir}/${config_file}.tmp

fi

fi


fi
